@extends('layouts.app')

@section('title')
{{ $options->title}}
@endsection


@section('content')


 <!-- автозапуск модального окна при успехе -->
<div class="modal" id="AllRightModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     @if (session('status'))
		 <script type="text/javascript">
          $(window).on('load',function(){
             $('#AllRightModal').modal('show');
             });   
         </script>
      <div class="modal-body text-center">
        <p class="text-success"> {{ session('status') }}</p>
      </div>
      @endif
    </div>
  </div>
</div>

<!-- автозапуск модального окна если есть ошибки-->
@if (count($errors->neworder) > 0)
   <script type="text/javascript">
 $(window).on('load',function(){
        $('#exampleModal').modal('show');
    });   
</script>
@endif 



<!-- для отображения имени файла в input type file (только в форме добавления новой записи)-->
	 <script type="text/javascript">
	$(document).ready( function() {
    $(".custom-file input[type=file]").change(function(){
        
          $("#newfilename").text('Файлы успешно добавлены');
       });
    });
    </script>


<script type="text/javascript">
function about(){
  $('html, body').animate({
    scrollTop: $("#about").offset().top - 50
  }, 1500);
};
function price(){
  $('html, body').animate({
    scrollTop: $("#price").offset().top - 50
  }, 1000);
};
function contacts(){
  $('html, body').animate({
    scrollTop:  $("#contacts").offset().top - 50
  }, 1000);
};
function portfolio(){
  $('html, body').animate({
    scrollTop: $("#portfolio").offset().top - 50
  }, 1000);
};
function stocks(){
  $('html, body').animate({
    scrollTop: $("#stocks").offset().top - 50
  }, 1000);
};
function reviews(){
  $('html, body').animate({
    scrollTop: $("#reviews").offset().top - 50
  }, 1000);
};
</script>

<script type="text/javascript">
$(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar-fixed-top");
    $nav.toggleClass('navbar-light', $(this).scrollTop() > $nav.height());  
    $nav.toggleClass('navbar-dark', $(this).scrollTop() < $nav.height());
    $nav.toggleClass('bg-light', $(this).scrollTop() > $nav.height());  
    $nav.toggleClass('bg-dark', $(this).scrollTop() < $nav.height());
  });
});
</script>


<script type="text/javascript">
$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
  if(!$this.hasClass('panel-collapsed')) {
    $this.parents('.panel').find('.panel-body').slideUp();
    $this.addClass('panel-collapsed');
    $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    
  } else {
    $this.parents('.panel').find('.panel-body').slideDown();
    $this.removeClass('panel-collapsed');
    $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    
  }
})
</script>

<script type="text/javascript">
$(document).ready( function() {
  @foreach ($priceItems as $priceItem)  
    $('#collapsePrice{{$priceItem->id}}').on('show.bs.collapse', function () {
       $('#i-collapse{{$priceItem->id}}').removeClass('fas fa-caret-down').addClass('fas fa-caret-up');
    });
    $('#collapsePrice{{$priceItem->id}}').on('hide.bs.collapse', function () {
       $('#i-collapse{{$priceItem->id}}').removeClass('fas fa-caret-up').addClass('fas fa-caret-down');
    });
       
@endforeach
});
</script>

<div class="container my-5 py-5 mainblock">
  <div class="row ">
     <div class="col-12 col-sm-12 col-md-12 text-center py-md-5">

        <h1 class="title text-uppercase">   
           <strong> {{$options->title}} </strong>
        </h1>
        <hr class="my-5">
        <h5 class="display-4 description">   
           @php  
          $des = nl2br($options->description);
          echo $des; 
          @endphp 
        </h5>
       <button type="button" class="btn btn-outline-primary btn-lg rounded mt-2" onClick="about()"> Узнать больше</button> 
     </div>
  </div>
</div>
 
<div class="container-fluid light" id="about">
  <div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center my-md-5 py-md-5 my-2 py-2">
       <strong>{{$options->description_full}}</strong>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center my-md-5 py-md-5 my-2 py-2">
      <button type="button" class="btn btn-outline-primary btn-lg rounded" data-toggle="modal" data-target="#exampleModal"> Отправить заявку на ремонт</button> 
    </div>
  </div>
</div>

<div class="backwhite">
<div class="container-fluid" id="price">
<!-- Прайс -->	
@if ($options->price == true) 
<div class="row">
 <div class="col-12 text-center">
  <h1 class="display-4">  Цены и услуги </h1>
 </div>
</div> 
@endif
 
@if (($options->price == true) && ($priceItems->isEmpty()))
Пока в прайсе нет записей...
@elseif (($options->price == true) && ($priceItems->isNotEmpty())) 
<div class="row price justify-content-center">
 <div class="col-8">	  
 @foreach ($priceItems as $priceItem) 
  <button class="btn  btn-light text-left btn-block " type="button" data-toggle="collapse" data-target="#collapsePrice{{$priceItem->id}}" aria-expanded="false" aria-controls="collapsePrice{{$priceItem->id}}">
   <div class="row">
    <div class="col-6 text-left">
        <h1 class="display-4 price-title">{{$priceItem->title}}</h1>
    </div>
    <div class="col-6 text-right">
        <i id="i-collapse{{$priceItem->id}}" class="fas fa-caret-down" style="font-size: 35px; color:#357ecc"></i>
    </div>
   </div>
  </button>
  <div class="collapse" id="collapsePrice{{$priceItem->id}}">
    <div class="card card-body">
     @php  
     $des = nl2br($priceItem->description);
     echo $des; 
     @endphp
    </div>
  </div>
 @endforeach
 </div>  
</div>  

@endif 
</div>

<div class="container-fluid" id="portfolio">
 
<!-- Портфолио -->  
@if ($options->portfolio == true) 
   <div class="row">
      <div class="col-12 text-center">
      <h1 class="display-4">  Портфолио </h1>
      </div>
     </div> 
@endif

@if (($options->portfolio == true) && ($portfolioItems->isEmpty()))
Пока в портфолио записей нет...
@elseif (($options->portfolio == true) && ($portfolioItems->isNotEmpty()))




    <section class="gallery-block compact-gallery">
        <div class="container">
            
            <div class="row no-gutters">
               @foreach ($portfolioItems as $portfolioItem) 
                <div class="col-md-6 col-lg-4 item zoom-on-hover">
                    <a class="lightbox" href="{{asset($portfolioItem->image)}}">
                        <img class="img-fluid image" src="{{asset($portfolioItem->image)}}">
                        <span class="description">
                            <span class="description-heading"> {{ $portfolioItem->title }}</span>
                            <span class="description-body"> {{ $portfolioItem->description }}</span>
                        </span>
                    </a>
                </div>
                 @endforeach  
            </div>
        </div>
    </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.compact-gallery', { animation: 'slideIn'});
    </script>

@endif  
</div>



 
<div class="container-fluid" id="stocks">
 <!-- Акции --> 
@if ($options->stocks == true) 
   <div class="row">
      <div class="col-12 text-center">
      <h1 class="display-4">  Акции </h1>
      </div>
    </div> 
@endif

@if (($options->stocks == true) && ($stocks->isEmpty()))
Пока акций нет...
@elseif (($options->stocks == true) && ($stocks->isNotEmpty()))


  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
           @foreach ($stocks as $stock) 
             <li data-target="#carouselExampleIndicators" data-slide-to="{{$stock->id}}" @if ($loop->first) class="active" @endif ></li>
           @endforeach  
        </ol>
        <div class="carousel-inner">
          @foreach ($stocks as $stock) 
            <div class="carousel-item rounded @if ($loop->first) active @endif">
              <div class="slideritem">
                <div class="row justify-content-center">
                  <div class="col-10 col-md-5">
                     <h3 class="stock-title">  {{$stock->title}} </h3>
                     <h1 class="display-4 stock-desc">   {{$stock->description}}  </h1> 
                  </div>
                  <div class="col-10 col-md-5">
                        @unless ($stock->image == "") 
                          <img class="img-fluid rounded" src="{{asset($stock->image)}}" >
                        @endunless
                  </div>
                </div>
              </div>
            </div>
          @endforeach  
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
@endif  
</div> 
 

 
 
 

 


 
  <div class="container-fluid" id="reviews">
<!-- Отзывы -->	
@if ($options->reviews == true) 
<div class="row">
 <div class="col-12 text-center">
 <h1 class="display-4"> Отзывы</h1>
 </div>
</div> 
@endif

@if (($options->reviews == true) && ($reviews->isEmpty()))
Пока отзывов нет...
@elseif (($options->reviews == true) && ($reviews->isNotEmpty()))
<div class="row">
 @foreach ($reviews as $review)
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">	  
   <blockquote class="blockquote text-center">
      <p class="mb-0">{{$review->review}}</p>
      <footer class="blockquote-footer">{{$review->name}}</footer>
    </blockquote>
 </div>
 @endforeach
</div>
@endif
     </div>

	 

<div class="container" id="contacts">
<!-- Контакты -->	
<div class="row">
 <div class="col-12 text-center">
 <h1 class="display-4"> Контакты </h1>

 </div>
</div> 

<div class="row">
  <div class="col-12 d-flex justify-content-center">	


   @php
   $map = htmlspecialchars_decode($options->map, ENT_QUOTES);
   print $map;
   @endphp
  

 </div>
 <div class="col-12 text-center my-3">	  
  {{$options->name_org}} </br>
  <a href="tel:+{{$options->phone_org}}">+{{$options->phone_org}}</a></br>
  {{$options->time}} </br>
  {{$options->adress_org}} </br>
  </br>
  <span class="contacts">
   @if (($options->v_on) == true)
  <a title="Viber" href="viber://add?number={{$options->v}}"><i class="fab fa-viber fa-3x mb-3 sr-contact-2"></i></a>
  @endif
  @if (($options->t_on) == true)
  <a title="Telegram" href="tg://resolve?domain={{$options->t}}"><i class="fab fa-telegram-plane fa-3x mb-3 sr-contact-2"></i></a>
  @endif
  @if (($options->w_on) == true)
  <a title="WhatsApp" href="whatsapp://send?phone=+{{$options->w}}"><i class="fab fa-whatsapp fa-3x mb-3 sr-contact-2"></i></a>
  @endif
</span>
 </div>	 
</div>  


 </div>  
  
 

</div>


@include('inc.footer')	
	




<!-- Модальное окно для добавления новой заявки -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">  
       <!-- Форма для добавления новой заявки -->
     <form method="post" action="{{ url('storeorder') }}" enctype="multipart/form-data" > 
       {{ csrf_field() }}

        <div class="row myform">
     <div class="col">
          <input  type="text"  name="name" class="form-control form-control-lg @if ($errors->neworder->has('name')) is-invalid @endif" value="@if($errors->neworder){{ old('name') }}@endif" placeholder="Ваше имя">        
     </div>
        </div>

        <div class="row myform">
     <div class="col">
          <input  type="tel"  name="phone" class="form-control form-control-lg @if ($errors->neworder->has('phone')) is-invalid @endif" value="@if($errors->neworder){{ old('phone') }}@endif" placeholder="Ваш телефон">       
     </div>
        </div>
     
     <div class="row myform">
     <div class="col">
           <textarea  type="text"  name="description" class="form-control form-control-lg @if ($errors->neworder->has('description')) is-invalid @endif" placeholder="Описание задачи">@if($errors->neworder){{old('description')}}@endif</textarea>          
     </div>
        </div>
       
  
       <div class="row myform">
     <div class="col">   
      <div class="custom-file">
            <input type="file"  multiple name="file[]"  class="file-upload custom-file-input @if ($errors->neworeder->has('image')) is-invalid @endif" id="validatedCustomFile">
            <label class="custom-file-label" id="newfilename" for="validatedCustomFile">Добавьте фото...</label>
      
          </div>          
     </div>
        </div>

    @include('common.errors')   
        
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
       <button class="btn btn-primary" type="submit">Отправить</button>  
   
     </form>
      
     </div>
    </div>
  </div>
</div>
<!-- конец модального окна -->
  </div>

@endsection
