@extends('layouts.app')

@section('title')
Администрирование | Настройки
@endsection

@section('content')



<!-- модальное окно поддтверждения сброса настроек -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Подтвердите действие</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       Все настройки будут сброшены
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
        <a href="{{ url('/setdefaultoptions') }}"> <button  type="button" class="btn btn-primary"> Сбросить настройки </button> </a>
      </div>
    </div>
  </div>
</div>
<!-- конец модального окна -->



 <div class="container">

		
<div class="row">
  <!-- Менюбар-->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   @include('inc.navbar')
  </div>
  <!-- Основной контент -->
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mycontent rounded">
   <div class=" ">
    <h4 class="display-6"> Настройки </h4>
	<hr>	
 <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-main-tab" data-toggle="pill" href="#pills-main" role="tab" aria-controls="pills-main" aria-selected="true">Основные</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-moduls-tab" data-toggle="pill" href="#pills-moduls" role="tab" aria-controls="pills-moduls" aria-selected="false">Модули</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contacts-tab" data-toggle="pill" href="#pills-contacts" role="tab" aria-controls="pills-contacts" aria-selected="false">Контакты</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-stocks-tab" data-toggle="pill" href="#pills-stocks" role="tab" aria-controls="pills-stocks" aria-selected="false">Акции</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-users-tab" data-toggle="pill" href="#pills-users" role="tab" aria-controls="pills-users" aria-selected="false">Пользователи</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade active show" id="pills-main" role="tabpanel" aria-labelledby="pills-main-tab">
    <h5> Основные настройки </h5>
	</br>
		   <form>

		    <div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Заголовок сайта</label>
              <div class="col-sm-4">
                 <input type="text" name="title" id="title" class="form-control" id="inputTitle" placeholder="Введите заголовок сайта" value="{{$options->title}}">
              </div>
            </div>
		    
			  <div class="form-group row">
              <label for="inputDesc" class="col-sm-3 col-form-label">Описание сайта</label>
              <div class="col-sm-4">
                 <textarea type="text" name="description" id="description"  placeholder="Введите описание сайта" class="form-control form-control-lg">{{$options->description}}</textarea>          
              </div>
          </div>

          <div class="form-group row">
              <label for="inputDesc" class="col-sm-3 col-form-label">Описание сайта (Полное)</label>
              <div class="col-sm-4">
                 <textarea type="text" name="description-full" id="descriptionfull"  placeholder="Введите описание сайта" class="form-control form-control-lg">{{$options->description_full}}</textarea>          
              </div>
          </div>
		   
			
		   	<div class="form-group row">
			   	<div class="col-3">
                  
                </div>
				<div class="col-lg-9 col-sm-12">
				<a href data-toggle="modal" data-target="#exampleModal"> Восстановить все настройки по умолчанию </a>
				</div>
			 </div>
			   </form>
			   <div class="form-group row">
                   <div class="col-sm-10">
	               <button id="savemain" class="btn btn-primary">Сохранить</button>  <img class="loading" id="loadicon" src="{{asset('img/loading.png')}}" />
                   </div>
              </div>
			 
              
				<div class="alert alert-danger alert-dismissible fade show" id="mainerrors" role="alert">
				
                </div>
				
				
				
  </div>
  
  <div class="tab-pane" id="pills-moduls" role="tabpanel" aria-labelledby="pills-moduls-tab">
  <h5>  Включение/отключение модулей </h5>
   <form>

   <div class="form-group form-check">
    <input type="checkbox" name="stocks" class="form-check-input"  id="stocks" @if ($options->stocks == '1') checked @endif>
    <label class="form-check-label" for="stocks"> Акции </label>
   </div>  
     
   <div class="form-group form-check">
    <input type="checkbox" name="price" class="form-check-input"  id="price" @if ($options->price == '1') checked @endif>
    <label class="form-check-label" for="price"> Цены и услуги </label>
   </div>
  
   <div class="form-group form-check">
    <input type="checkbox" name="portfolio" class="form-check-input"  id="portfolio" @if ($options->portfolio == '1') checked @endif>
    <label class="form-check-label" for="portfolio"> Портфолио </label>
   </div>

   <div class="form-group form-check">
    <input type="checkbox" name="reviews" class="form-check-input"  id="reviews" @if ($options->reviews == '1') checked @endif>
    <label class="form-check-label" for="reviews"> Отзывы </label>
   </div>
  </form>
   <div class="form-group row">
        <div class="col-sm-10">
	      <button id="savemoduls" class="btn btn-primary">Сохранить</button>   <img class="loading" id="loadiconm" src="{{asset('img/loading.png')}}" />
        </div>
    </div>
	


   
  </div>
  <div class="tab-pane fade" id="pills-contacts" role="tabpanel" aria-labelledby="pills-contacts-tab">
     <h5> Контакты </h5>
	 
	 <form>
	   
            <div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Название организации</label>
              <div class="col-sm-6">
                 <input type="text" name="name_org" class="form-control" id="name_org" placeholder="Введите название организации" value="{{$options->name_org}}">
              </div>
            </div>
			
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Телефон</label>
              <div class="col-sm-6">
			   <div class="input-group-prepend">
			      <span class="input-group-text" id="inputGroupPrepend">+</span>
                 <input type="tel" name="phone_org" class="form-control" id="phone_org" placeholder="Введите телефон"  value="{{$options->phone_org}}">
              </div>
			 </div>
            </div>
			
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">График работы</label>
              <div class="col-sm-6">
                 <input type="text" class="form-control" id="time" placeholder="Введите график работы" value="{{$options->time}}">
              </div>
            </div>
		    
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Адрес</label>
              <div class="col-sm-6">
                 <input type="text" name="adress_org" class="form-control" id="adress_org" placeholder="Введите адрес"  value="{{$options->adress_org}}">
              </div>
            </div>
			 
			 
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Карта</label>
              <div class="col-sm-6">
			    <textarea type="text" id="map" class="form-control form-control-lg" placeholder="Вставьте сюда скрипт карты" >{{$options->map}}</textarea>           
              </div>
            </div>

			
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Viber</label>
              <div class="col-sm-5">
			  <div class="input-group-prepend">
			      <span class="input-group-text" id="inputGroupPrepend">+</span>
                  <input type="tel" name="v" class="form-control" id="v" placeholder="Введите телефон Viber"  value="{{$options->v}}">
              </div>           
			 </div>
            </div>
			
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">WhatsApp</label>
              <div class="col-sm-5">
			   <div class="input-group-prepend">
			      <span class="input-group-text" id="inputGroupPrepend">+</span>
                  <input type="tel" name="w" class="form-control" id="w" placeholder="Введите телефон WhatsApp"  value="{{$options->w}}">
                </div> 
			  </div>
            </div>

			
			 
			<div class="form-group row">
              <label for="inputTitle" class="col-sm-3 col-form-label">Ник Telegram</label>
              <div class="col-sm-5">
			   <div class="input-group-prepend">
                 <span class="input-group-text" id="inputGroupPrepend">@</span>
			     <input type="tel" name="t" class="form-control" id="t" placeholder="Введите ник Telegram"  value="{{$options->t}}">
               </div> 
			 </div>
            </div>

			<div class="form-group form-check">
              <input type="checkbox" name="v_on" class="form-check-input"  id="v_on" @if ($options->v_on == '1') checked @endif>
              <label class="form-check-label" for="v_on"> Показывать Viber </label>
            </div>
			
			<div class="form-group form-check">
              <input type="checkbox" name="w_on" class="form-check-input"  id="w_on" @if ($options->w_on == '1') checked @endif>
              <label class="form-check-label" for="w_on"> Показывать WhatsApp </label>
            </div>
			
			<div class="form-group form-check">
              <input type="checkbox" name="t_on" class="form-check-input"  id="t_on" @if ($options->t_on == '1') checked @endif>
              <label class="form-check-label" for="t_on"> Показывать Telegram</label>
            </div>
		</form>
			<div class="form-group row">
              <div class="col-sm-10">
	           <button id="savecontacts" class="btn btn-primary">Сохранить</button> <img class="loading" id="loadiconc" src="{{asset('img/loading.png')}}" />
              </div>
            </div>
        
               <div class="alert alert-danger alert-dismissible fade show" id="contactserrors" role="alert">
				
                </div>
				
    
  </div>
  <div class="tab-pane fade" id="pills-stocks" role="tabpanel" aria-labelledby="pills-stocks-tab">
    <h5> Акции </h5>	
      <form>
      Показывать акции за  <input type="number" id="stock_visible_before" name="stock_visible_before" class="formwidth" placeholder="" value="{{$options->stock_visible_before}}">  дней до их начала
	</br> &nbsp
	</form>
	<div class="form-group row">
          <div class="col-sm-10">
	      <button id="savestocks" class="btn btn-primary">Сохранить</button> <img class="loading" id="loadicons" src="{{asset('img/loading.png')}}" />
    </div>
	
	 
  </div>
  
   <div class="alert alert-danger alert-dismissible fade show" id="stockserrors" role="alert">
   </div>
</div>




<div class="tab-pane fade" id="pills-users" role="tabpanel" aria-labelledby="pills-users-tab">
    <h5> Пользователи </h5>  
   
  <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Имя</th>
      <th scope="col">Email</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>

     

    @foreach ($users as $user)
    <form action="{{ url('/deleteuser/'.$user->id) }}" method="POST">
             {{ csrf_field() }}
             {{ method_field('DELETE') }}
             
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td><button type="submit" class="btn btn-danger @if (Auth::user()->id === $user->id) disabled @endif"  @if (Auth::user()->id === $user->id) disabled @endif >Удалить</button></td>
    </tr>

     </form>
    @endforeach
  </tbody>
</table>
    

<hr>
 <h5> Добавить нового пользователя</h5>  
<form action="{{ url('/createuser/') }}" method="POST">
             {{ csrf_field() }}


   <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Имя</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @if ($errors->newuser->has('name')) is-invalid @endif" name="name" value="{{ old('name') }}" required autofocus>

                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @if ($errors->newuser->has('email')) is-invalid @endif" name="email" value="{{ old('email') }}" required>

                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @if ($errors->newuser->has('password')) is-invalid @endif" name="password" required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right @if ($errors->newuser->has('password')) is-invalid @endif">Пароль еще раз</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0 mb-5">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Добавить
                                </button>
                            </div>
                        </div>

                        @include('common.errors') 
</form>

      
                     
                  
</div>








   </div>	 
  </div> 
</div> 

<script src="{{asset('js/options.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@endsection

