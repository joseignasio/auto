<!-- resources/views/inc/navbar.blade.php -->


<div class="mynavbar rounded d-none d-md-block">
 <ul class="nav flex-column nav-pills">
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'portfolio') active @endif" href="{{ url('/dashboard/portfolio') }}">Портфолио  <span class="badge @if(Route::currentRouteName() == 'portfolio') badge-light @else badge-primary @endif badge-pill">{{$portfolioItemsCount}}</span> </a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'price') active @else @endif" href="{{ url('/dashboard/price') }}">Цены и услуги <span class="badge @if(Route::currentRouteName() == 'price') badge-light @else badge-primary @endif badge-pill">{{$priceItemsCount}}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'orders') active @else @endif" href="{{ url('/dashboard/orders') }}">Заявки <span class="badge @if(Route::currentRouteName() == 'orders') badge-light @else badge-primary @endif badge-pill">{{$ordersCount}}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'reviews') active @else @endif" href="{{ url('/dashboard/reviews') }}">Отзывы <span class="badge @if(Route::currentRouteName() == 'reviews') badge-light @else badge-primary @endif badge-pill">{{$reviewsCount}}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'stocks') active @else @endif" href="{{ url('/dashboard/stocks') }}">Акции <span class="badge @if(Route::currentRouteName() == 'stocks') badge-light @else badge-primary @endif badge-pill">{{$stocksCount}}</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if (Route::currentRouteName() == 'options') active @else @endif" href="{{ url('/dashboard/options') }}">Настройки</a>
  </li>
 </ul>
</div>

