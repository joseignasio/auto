


<div class="footer p-2">
<div class="container">
<div class="row">
 <div class="col-lg-4 col-md-4 col-sm-6 col-6">	  
  <ul class="nav flex-column">
   @if ($options->price == true) 
   <li class="nav-item">
     <button type="button" class="btn btn-link nav-link" onClick="price()" >Цены и услуги </button>
   </li>
   @endif
   @if ($options->portfolio == true) 
   <li class="nav-item">
     <button type="button" class="btn btn-link nav-link" onClick="portfolio()">Портфолио </button>
   </li>
   @endif
   @if ($options->stocks == true) 
   <li class="nav-item">
    <button type="button" class="btn btn-link nav-link" onClick="stocks()"> Акции </button>
   </li>
   @endif
   @if ($options->reviews == true) 
   <li class="nav-item">
    <button type="button" class="btn btn-link nav-link" onClick="reviews()"> Отзывы </button>
   </li>
   @endif
   <li class="nav-item">
    <button type="button" class="btn btn-link nav-link" onClick="contacts()"> Контакты </button>
   </li>
  </ul>
 </div> 
 <div class="col-lg-4 col-md-4 col-sm-6 col-6">   
  <ul class="nav flex-column">
   <li class="nav-item">
    <a class="nav-link text-secondary" href data-toggle="modal" data-target="#exampleModal">Создать заявку на ремонт</a>
   </li>
   <li class="nav-item text-secondary">
    <a href class="nav-link text-secondary" data-toggle="modal" data-target="#exampleModalreview"> Оставить отзыв </a>
   </li>
 </div> 
 <div class="col-lg-4 col-md-4 col-sm-6 col-6">   
   </ul>
  <ul class="nav flex-column">
   <li class="nav-item">
    <a class="nav-link text-secondary" href="{{ url('/dashboard/portfolio') }}">Администрирование</a>
   </li>
  </ul>
 </div> 
</div>
</div>
</div>





 <!-- Модальное окно для добавления отзыва -->
<div class="modal fade" id="exampleModalreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">  
       <!-- Форма для добавления отзыва -->
     <form method="post" action="{{ url('storereviewbyany') }}" enctype="multipart/form-data">
       {{ csrf_field() }}

        <div class="row myform">
     <div class="col">
          <input  type="text"  name="name" class="form-control form-control-lg @if ($errors->newreview->has('name')) is-invalid @endif" value="@if($errors->newreview){{ old('name') }}@endif" placeholder="Ваше имя">        
     </div>
        </div>

        <div class="row myform">
     <div class="col">
          <textarea  type="text" name="review" class="form-control form-control-lg @if ($errors->newreview->has('review')) is-invalid @endif" placeholder="Ваш отзыв">@if($errors->newreview){{old('review')}}@endif</textarea>         
     </div>
        </div>
     
     
    
     
    @include('common.errors')   
        
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
       <button class="btn btn-primary" type="submit">Отправить</button>  
     </form>
      
    
     </div>
    </div>
  </div>
</div>
<!-- конец модального окна -->  