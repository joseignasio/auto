
 

<nav class=" sticky-top navbar-fixed-top navbar navbar-expand-lg @if(Request::is('dashboard/*')) navbar-light bg-light  @else  navbar-dark bg-dark  @endif  " id="menu">

  <a class="navbar-brand" href="#">{{-- @if(Request::is('dashboard/*')) @else {{ $options->title or '' }} @endif --}}</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse mx-auto" id="navbarText">
    <ul class="navbar-nav mx-auto">
@if(Request::is('dashboard/*'))

@else
      @if ($options->price == true) 
      <li class="nav-item">
        <button type="button" class="btn btn-link nav-link"  onClick="price()">Услуги </button>
      </li>
      @endif
      @if ($options->portfolio == true) 
      <li class="nav-item">
        <button type="button" class="btn btn-link nav-link"  onClick="portfolio()">Портфолио </button>
      </li>
      @endif
      @if ($options->stocks == true) 
      <li class="nav-item">
        <button type="button" class="btn btn-link nav-link"  onClick="stocks()"> Акции </button>
      </li>
      @endif
      @if ($options->reviews == true) 
      <li class="nav-item">
        <button type="button" class="btn btn-link nav-link"  onClick="reviews()"> Отзывы </button>
      </li>
      @endif
	    <li class="nav-item">
        <button type="button" class="btn btn-link nav-link"  onClick="contacts()"> Контакты </button>
      </li>
@endif
@if (Auth::guest())

@else
	   <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="{{ url('/dashboard/portfolio') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Администрирование
        </a>
        <div class="dropdown-menu mydropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item @if (Route::currentRouteName() == 'portfolio')  list-group-item-primary @endif" href="{{ url('/dashboard/portfolio') }}"> Портфолио  <span class="badge badge-primary badge-pill">{{$portfolioItemsCount}}</span></a>
          <a class="dropdown-item @if (Route::currentRouteName() == 'price') list-group-item-primary   @endif" href="{{ url('/dashboard/price') }}"" href="#"> Цены и услуги <span class="badge badge-primary badge-pill">{{$priceItemsCount}}</span></a>
          <a class="dropdown-item @if (Route::currentRouteName() == 'orders') list-group-item-primary  @endif" href="{{ url('/dashboard/orders') }}"> Заявки <span class="badge badge-primary badge-pill">{{$ordersCount}}</span></a>
          <a class="dropdown-item @if (Route::currentRouteName() == 'reviews') list-group-item-primary  @endif" href="{{ url('/dashboard/reviews') }}"> Отзывы <span class="badge badge-primary badge-pill">{{$reviewsCount}}</span></a>
          <a class="dropdown-item @if (Route::currentRouteName() == 'stocks') list-group-item-primary  @endif" href="{{ url('/dashboard/stocks') }}"> Акции <span class="badge badge-primary badge-pill">{{$stocksCount}}</span></a>
          <a class="dropdown-item @if (Route::currentRouteName() == 'options') list-group-item-primary @endif" href="{{ url('/dashboard/options') }}">Настройки</a>
    	</div>
      </li>
	   <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}">Главная страница</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/logout') }}">Выйти</a>
      </li>

@endif 	  
    </ul>
    <span class="navbar-text mr-0">
      @if(Request::is('dashboard/*'))  Вы вошли как: <strong> {{ Auth::user()->name }} </strong>  @endif
    </span>
  </div>
</nav> 

  
  
  
<script>
$(document).ready(function(){
  

$('.price').on('click', function(e){
  $('html,body').stop().animate({ scrollTop: $('#price').offset().top }, 1000);
  e.preventDefault();
});



});
</script>

