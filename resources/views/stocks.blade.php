@extends('layouts.app')

@section('title')
Администрирование | Акции
@endsection

@section('content')
 <div class="container">



 <!-- автозапуск модального окна при успехе -->
<div class="modal" id="AllRightModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     @if (session('status'))
		 <script type="text/javascript">
          $(window).on('load',function(){
             $('#AllRightModal').modal('show');
             });   
         </script>
      <div class="modal-body text-center">
        <p class="text-success"> {{ session('status') }}</p>
      </div>
      @endif
    </div>
  </div>
</div>

<!-- автозапуск модального окна если есть ошибки-->
@if (count($errors->newstock) > 0)
   <script type="text/javascript">
 $(window).on('load',function(){
        $('#exampleModal').modal('show');
    });   
</script>
@endif 


<!-- для отображения имени файла в input type file (только в форме добавления новой записи)-->
	 <script>
	$(document).ready( function() {
    $(".custom-file input[type=file]").change(function(){
         var filename = $(this).val().replace(/.*\\/, "");
          $("#newfilename").text(filename);
       });
    });
    </script>	

<!-- Модальное окно для добавления новой записи в прайс-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавить акцию</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
       <!-- Форма для новой записи в портфолио -->
	   <form method="post" action="{{ url('/storestock') }}" enctype="multipart/form-data">
       {{ csrf_field() }}	  	 


	   <div class="row myform">
		 <div class="col">
          <input  type="text"  name="title" class="form-control form-control-lg @if ($errors->newstock->has('title')) is-invalid @endif" value="@if ($errors->newstock){{old('title')}}@endif" placeholder="Название акции">    	  
		 </div>
       </div>
	   
       <div class="row myform">
		 <div class="col">
          <textarea type="text" name="description" class="form-control form-control-lg @if ($errors->newstock->has('description')) is-invalid @endif" placeholder="Описание акции" >@if($errors->newstock){{ old('description')}}@endif</textarea>           
		 </div>
        </div>
	   
        <div class="row myform">
		 <div class="col">
          <input type="date" name="date_start" class="form-control form-control-lg @if ($errors->newstock->has('date_start')) is-invalid @endif" value="@if ($errors->newstock) {{ old('date_start') }} @endif@" >         
		 </div>
        </div>
		
		 <div class="row myform">
		 <div class="col">
          <input type="date" name="date_end" class="form-control form-control-lg @if ($errors->newstock->has('date_end')) is-invalid @endif" value="@if ($errors->newstock) {{ old('date_end') }} @endif@" >         
		 </div>
        </div>
		
		<div class="row myform">
		 <div class="col">	 
		  <div class="custom-file">
            <input type="file"  name="image"  class="file-upload custom-file-input @if ($errors->newstock->has('image')) is-invalid @endif" id="validatedCustomFile">
            <label class="custom-file-label" id="newfilename" for="validatedCustomFile">Выберите фото...</label>
			
          </div>          
		 </div>
        </div>
		
        
		<div class="row myform">
		 <div class="col">
		  <div class="form-check">
           <input name="public" class="form-check-input"  type="checkbox" id="myCheck" checked="checked">
           <label class="form-check-label" for="myCheck"> Публиковать  </label>
          </div>
		 </div>		
	    </div>
		
		
				   
<!-- Отображение ошибок проверки ввода -->
@if  (session('formnumber') == null) @include('common.errors') @endif	
		
		
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
       <button class="btn btn-primary" type="submit">Добавить акцию</button>  
	   </form>
     </div>
    </div>
  </div>
</div>
<!-- конец модального окна -->


		
<div class="row">
  <!-- Менюбар-->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   @include('inc.navbar')
  </div>
  <!-- Основной контент -->
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mycontent rounded">
   <div class="">
	<div class="row"> 
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 
		<!--контейнер для кнопки Добавить запись --> 
	  <div class="row"> 
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <h4 class="display-6"> Акции </h4>
	<hr>
	    <button type="button" class="btn btn-primary addbtn" data-toggle="modal" data-target="#exampleModal"> Добавить акцию </button> 
	   </div>
	  </div>
		 <!--записи --> 
	  	 
      @if ($stocks->isEmpty()) Пока акиций нет. Нажмите кнопку "Добавить акцию"
	  @endif	  
	  
      @foreach ($stocks as $stock) 
	  
	  
	  
<!-- для отображения имени файла в input type file (только в форме изменения записи)-->
	 <script>
	$(document).ready( function() {
    $(".myfile{{$stock->id}} input[type=file]").change(function(){
         var filename = $(this).val().replace(/.*\\/, "");
		 $("#filename{{$stock->id}}").text(filename);
       });
    });
    </script>	


	     <div class="price-item rounded">
	        
		      <strong> Название акции: </strong>  {{ $stock->title }}    &nbsp
              <br> <strong> Описание акции: </strong> {{ $stock->description }}       &nbsp
			  <br> <strong> Время действия акции: </strong>	 {{$stock->date_start}} -  {{$stock->date_end}}		  &nbsp
		      <br>  <strong> Статус:  </strong>@if ($stock->public  == 1) Опубликовано @else Не опубликовано @endif  <br>
		  @if (($stock->image) != null) <div class="stock-img"><img src="{{asset($stock->image)}}" class="rounded"> </div> @endif
		   <form action="{{ url('/deletestock/'.$stock->id) }}" method="POST">
             {{ csrf_field() }}
             {{ method_field('DELETE') }}
             
			 
			 <!-- кнопки -->
             <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter{{$stock->id}}">Удалить</button>
             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changeModalfor{{$stock->id}}"> Изменить </button> 
			 
               <!-- Модальное окно Подтверждение удаления -->
               <div class="modal fade" id="exampleModalCenter{{$stock->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                 <div class="modal-content">
                  <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">Подтвердите действие</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body text-center">
                    Вы уверены? <p> <p>
		            <button type="submit" id="delete-task-{{ $stock->id }}" class="btn btn-danger">Удалить </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
				  </div>
                 </div>
                </div>
               </div>
		   </form> 	 
		   
		 </div> 	
		 
<!-- определяем переменную form которая содержит номер формы в которой провалилась валидация-->
@php $form = 'changestock' . $stock->id;  @endphp 		  
<!-- автозапуск модального окна если есть ошибки в ИЗМЕНЕНИИ записи-->
@if (count($errors->$form) > 0)
   <script type="text/javascript">
 $(window).on('load',function(){
        $('#changeModalfor{{ session('formnumber') }}').modal('show');
    });   
</script>
@endif 

		 
		 
		 
		<!-- Модальное окно для изменения записи в прайсе -->
		<div class="modal fade" id="changeModalfor{{$stock->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Изменить акцию</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
               </div>
			   
               <div class="modal-body">
               <!-- Форма для изменения записи в портфолио -->
	           <form method="post" action="{{ url('changestock/'.$stock->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}	 

				
				
				
				
				<div class="row myform">
		 <div class="col">
          <input  type="text"  name="title" class="form-control form-control-lg @if ($errors->$form->has('title')) is-invalid @endif" value="@if (count($errors->$form) > 0) {{ old('title') }} @else {{ $stock->title }} @endif" placeholder="Название акции">    	  
		 </div>
       </div>
	   
       <div class="row myform">
		 <div class="col">
          <textarea type="text" name="description" class="form-control form-control-lg @if ($errors->$form->has('description')) is-invalid @endif" placeholder="Описание акции" >@if(count($errors->$form) > 0){{ old('description') }} @else {{ $stock->description }} @endif</textarea>           
		 </div>
        </div>
	   
        <div class="row myform">
		 <div class="col">
          <input type="date" name="date_start" class="form-control form-control-lg @if ($errors->$form->has('date_start')) is-invalid @endif" value="@if(count($errors->$form) > 0){{ old('date_start') }} @else{{ $stock->date_start }}@endif" >         
		 </div>
        </div>
		
		 <div class="row myform">
		 <div class="col">
          <input type="date" name="date_end" class="form-control form-control-lg @if ($errors->$form->has('date_end')) is-invalid @endif" value="@if(count($errors->$form) > 0){{ old('date_end') }} @else{{ $stock->date_end }}@endif" >         
		 </div>
        </div>
		
		<div class="row myform">
		 <div class="col">	 
		  <div class="custom-file myfile{{$stock->id}}">
            <input type="file"  name="image"  class="file-upload custom-file-input @if ($errors->$form->has('image')) is-invalid @endif" id="validatedCustomFile">
            <label class="custom-file-label" id="filename{{$stock->id}}" for="validatedCustomFile">Выберите фото...</label>
			
          </div>          
		 </div>
        </div>
		
        
		<div class="row myform">
		 <div class="col">
		  <div class="form-check">
           <input name="public" class="form-check-input"  type="checkbox" id="myCheck" @if ($stock->public  == 1) checked="checked" @else @endif>
           <label class="form-check-label" for="myCheck"> Публиковать  </label>
        	<input type="text" name="formnumber" value="{{$stock->id}}" hidden> <!-- скрытое  поле для идентификации формы с которой отправлен запрос-->
          </div>
		 </div>		
	    </div>
				
				
				
<!-- Отображение ошибок проверки ввода -->
@if  (session('formnumber') == $stock->id) @include('common.errors') @endif		       
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                  <button class="btn btn-primary" type="submit">Изменить акцию</button>  
	            </form>
                </div>
             </div>
           </div>
        </div>
      @endforeach
      </div> 
     </div> 
    </div> 
  </div> 
  </div>
@endsection

