@extends('layouts.app')

@section('title')
Администрирование | Прайс
@endsection

@section('content')
 <div class="container">



 <!-- автозапуск модального окна при успехе -->
<div class="modal" id="AllRightModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     @if (session('status'))
		 <script type="text/javascript">
          $(window).on('load',function(){
             $('#AllRightModal').modal('show');
             });   
         </script>
      <div class="modal-body text-center">
        <p class="text-success"> {{ session('status') }}</p>
      </div>
      @endif
    </div>
  </div>
</div>

<!-- автозапуск модального окна если есть ошибки-->
@if (count($errors->newprice) > 0)
   <script type="text/javascript">
 $(window).on('load',function(){
        $('#exampleModal').modal('show');
    });   
</script>
@endif 



<!-- Модальное окно для добавления новой записи в прайс-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавить новую запись</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">  
       <!-- Форма для новой записи в портфолио -->
	   <form method="post" action="{{ url('/storepriceitem') }}" enctype="multipart/form-data">
       {{ csrf_field() }}	  	 


	   <div class="row myform">
		 <div class="col">
          <input  type="text"  name="title" class="form-control form-control-lg @if ($errors->newprice->has('title')) is-invalid @endif" value="@if(session('formnumber') == null){{ old('title') }}@endif" placeholder="Название категории">    	  
		 </div>
       </div>
	   
       <div class="row myform">
		 <div class="col">
          <textarea type="text" name="description" class="form-control form-control-lg  @if ($errors->newprice->has('description')) is-invalid @endif" placeholder="Впишите сюда прайс лист" >@if(session('formnumber') == null){{old('description')}}@endif</textarea>           
		 </div>
        </div>
	   
        
        
		<div class="row myform">
		 <div class="col">
		  <div class="form-check">
           <input name="public" class="form-check-input"  type="checkbox" id="myCheck" checked="checked">
           <label class="form-check-label" for="myCheck"> Публиковать  </label>
          </div>
		 </div>		
	    </div>
		
		
				   
<!-- Отображение ошибок проверки ввода -->
@if  (session('formnumber') == null) @include('common.errors') @endif	
		
		
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
       <button class="btn btn-primary" type="submit">Добавить запись</button>  
	   </form>
     </div>
    </div>
  </div>
</div>
<!-- конец модального окна -->


		
<div class="row">
  <!-- Менюбар-->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   @include('inc.navbar')
  </div>
  <!-- Основной контент -->
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mycontent rounded">
   <div class="">
	<div class="row"> 
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 
		<!--контейнер для кнопки Добавить запись --> 
	  <div class="row"> 
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	   <h4 class="display-6"> Цены и услуги </h4>
	 <hr>
	    <button type="button" class="btn btn-primary addbtn" data-toggle="modal" data-target="#exampleModal"> Добавить запись </button> 
	   </div>
	  </div>
		 <!--записи --> 
	  	 
      @if ($items->isEmpty()) Пока в прайсе записей нет. Нажмите кнопку "Добавить запись"
	  @endif	  
	  
      @foreach ($items as $item) 

	     <div class="price-item rounded">
	        
		       <strong> Имя категории: </strong>  {{ $item->title }}    &nbsp
              <br> <strong>  Прайс категории: </strong> {{ $item->description }}       &nbsp
		      <br>  <strong> Статус:  </strong>@if ($item->public  == 1) Опубликовано @else Не опубликовано @endif  <br>
		    
		   <form action="{{ url('/deletepriceitem/'.$item->id) }}" method="POST">
             {{ csrf_field() }}
             {{ method_field('DELETE') }}
             
			 
			 <!-- кнопки -->
             <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter{{$item->id}}">Удалить</button>
             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changeModalfor{{$item->id}}"> Изменить </button> 
			 
               <!-- Модальное окно Подтверждение удаления -->
               <div class="modal fade" id="exampleModalCenter{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                 <div class="modal-content">
                  <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">Подтвердите действие</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body text-center">
                    Вы уверены? <p> <p>
		            <button type="submit" id="delete-task-{{ $item->id }}" class="btn btn-danger">Удалить </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
				  </div>
                 </div>
                </div>
               </div>
		   </form> 	 
		   
		 </div> 	
		 
<!-- определяем переменную form которая содержит номер формы в которой провалилась валидация-->
@php $form = 'changeprice' . $item->id;  @endphp 		  
<!-- автозапуск модального окна если есть ошибки в ИЗМЕНЕНИИ записи-->
@if (count($errors->$form) > 0)
   <script type="text/javascript">
 $(window).on('load',function(){
        $('#changeModalfor{{ session('formnumber') }}').modal('show');
    });   
</script>
@endif 

		 
		 
		 
		<!-- Модальное окно для изменения записи в прайсе -->
		<div class="modal fade" id="changeModalfor{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Изменить запись</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
               </div>
			   
               <div class="modal-body">
               <!-- Форма для изменения записи в портфолио -->
	           <form method="post" action="{{ url('changepriceitem/'.$item->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}	 

                <div class="row myform">
		          <div class="col">
                   <input  type="text"  name="title" class="form-control form-control-lg @if ($errors->$form->has('title')) is-invalid @endif" @if (count($errors->$form) > 0) value="{{ old('title') }}" @else value="{{ $item->title }}" @endif  placeholder="Название категории">    	  
				   </div>
                </div>

                <div class="row myform">
		         <div class="col">
                    <textarea type="text" name="description" class="form-control form-control-lg @if ($errors->$form->has('description')) is-invalid @endif" placeholder="Впишите сюда прайс">@if(count($errors->$form) > 0){{old('description')}}@else{{$item->description}}@endif</textarea>           
		         </div>
                </div>
				
            
                <div class="row myform">
		         <div class="col">
		         <div class="form-check">
                    <input name="public" class="form-check-input"  type="checkbox" id="myCheck"  @if ($item->public  == 1) checked="checked" @else @endif>
                    <label class="form-check-label" for="myCheck"> Публиковать  </label>
					<input type="text" name="formnumber" value="{{$item->id}}" hidden> <!-- скрытое  поле для идентификации формы с которой отправлен запрос-->
                 </div>
		        </div>		
	            </div>
			   
<!-- Отображение ошибок проверки ввода -->
@if  (session('formnumber') == $item->id) @include('common.errors') @endif		       
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                  <button class="btn btn-primary" type="submit">Изменить запись</button>  
	            </form>
                </div>
             </div>
           </div>
        </div>
      @endforeach
      </div> 
     </div> 
    </div> 
  </div> 
  </div>
@endsection

