@extends('layouts.app')

<!-- Main Content -->
@section('content')

<div class="text-center upbar">
  <a href="{{url('/')}}" class="btn btn-link">
    Главная
  </a>
   <a href="{{url('/login')}}" class="btn btn-link">
    Вход
  </a>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-9 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Сброс пароля</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Введите E-Mail</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                     Сбросить пароль
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
