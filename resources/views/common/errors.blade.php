<!-- resources/views/common/errors.blade.php -->


@if (count($errors->newuser->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->newuser->all() as $error)
        <li>{{ $error}}</li>
      @endforeach   
    </ul>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->newportfolioitem->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->newportfolioitem->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->newprice->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->newprice->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->neworder->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->neworder->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->newreview->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->newreview->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->newstock->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->newstock->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->main->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->main->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->contacts->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->contacts->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->stocks->all()) > 0)
  <!-- Список ошибок формы -->
  
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
      @foreach ($errors->stocks->all() as $error)
        <li>{{ $error}}</li>
      @endforeach	  
    </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif
<!-- ошибки в однотипных формах изменения записей -->

@php
 $portformerr = 'changeportfolioitem' . session('formnumber') ; 
 $priceformerr = 'changeprice' . session('formnumber') ; 
 $reviewformerr = 'changereview' . session('formnumber') ;
 $stockformerr = 'changestock' . session('formnumber') ;  
@endphp 
@if (count($errors->$portformerr->all()) > 0)
	<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
  @foreach ($errors->$portformerr->all() as $error)
          <li>{{ $error}}</li>
  @endforeach
   </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

@if (count($errors->$priceformerr->all()) > 0)
	<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
  @foreach ($errors->$priceformerr->all() as $error)
          <li>{{ $error}}</li>
  @endforeach
   </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

  
@if (count($errors->$reviewformerr->all()) > 0)
	<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
  @foreach ($errors->$reviewformerr->all() as $error)
          <li>{{ $error}}</li>
  @endforeach
   </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif

 
@if (count($errors->$stockformerr->all()) > 0)
	<div class="alert alert-danger alert-dismissible fade show" role="alert">

    <strong> Внимание! </strong>

    <br><br>

    <ul>
  @foreach ($errors->$stockformerr->all() as $error)
          <li>{{ $error}}</li>
  @endforeach
   </ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
@endif
  