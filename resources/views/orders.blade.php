@extends('layouts.app')

@section('title')
Администрирование | Заявки
@endsection

@section('content')
 <div class="container">

<!-- Отображение ошибок проверки ввода -->
 @include('common.errors')
	
<div class="row">
  <!-- Менюбар-->
  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
   @include('inc.navbar')
  </div>
  
  <!-- Основной контент -->
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mycontent rounded">
   <div class="">
	<div class="row"> 
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 
	<h4 class="display-6"> Заявки </h4>
	<hr>
	
		 <!--записи --> 
      @if ($orders->isEmpty()) Пока заявок нет...
	  @endif	  
	  
      @foreach ($orders as $order) 

	     <div class="order-item float-left rounded">
		 @php 	 
		 $imgarr = $order->image;
		 @endphp
		 <div class="row">
		 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
		  
		 @foreach ($imgarr as $img)
		    
	           <div class="oimg">
		            <a data-fancybox="gallery" href="{{asset($img)}}"><img src="{{asset($img)}}" class="border img-fluid rounded"></a>
	            </div>
	        
			
		 @endforeach 
		  </div>
		 </div>
		   <div class="d-inline-block text-truncate" style="max-width: 80%;">
              <br><strong>Имя:</strong> {{ $order->name }} &nbsp
		      <br><strong>Телефон:</strong> {{ $order->phone }}
              <br><strong>Дата размещения:</strong> {{ $order->created_at}}  &nbsp
			  <br><strong>Описание проблемы</strong>   
			
			 <div class="order-text">
			 @php  
  $des = nl2br($order->description);
  echo $des; 
  @endphp           &nbsp
          </div>
		   </div>
		   <form action="{{ url('deleteorder/'.$order->id) }}" method="POST">
             {{ csrf_field() }}
             {{ method_field('DELETE') }}
            
			 
			 <!-- кнопка Подтверждение удаления -->
             <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter{{$order->id}}">Удалить</button>
            
			 
               <!-- Модальное окно Подтверждение удаления -->
               <div class="modal fade" id="exampleModalCenter{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                 <div class="modal-content">
                  <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">Подтвердите действие</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body text-center">
                    Вы уверены? <p> <p>
		            <button type="submit" id="delete-task-{{ $order->id }}" class="btn btn-danger">Удалить </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
				  </div>
                 </div>
                </div>
               </div>
		   </form> 	  
		 </div> 	



      @endforeach
   </div> 
      </div> 
    </div> 
  </div> 
  </div>
@endsection

