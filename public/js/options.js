﻿

function enable() {
	$('#savemain').removeAttr('disabled');
	$("#savemain").html("Сохранить");
}

$( "#title" ).change(enable);
$( "#description" ).change(enable);
$( "#descriptionfull" ).change(enable);

$("#savemain").click(function(){
	$("#loadicon").show();
	$("#savemain").attr({
                  disabled: "disabled",
                });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var tit = $("#title").val();
	var desc = $("#description").val();
	var descFull = $("#descriptionfull").val();
	var nform = "main";
	$('#mainerrors').html('');
	$("#mainerrors").hide("slow");
	$("#title").removeClass("is-invalid");
	$("#description").removeClass("is-invalid");
    $("#descriptionFull").removeClass("is-invalid");
    $.ajax(
    {
        url: "/changeoptions",
        type: 'post', 
        data: {
            title: tit,
			description: desc,
			descriptionfull: descFull,
			form: nform
        },
        success: function (data)
        {    
		    $("#loadicon").hide();
       		if($.isEmptyObject(data.errors)){
		        $("#savemain").html(data.success);
				$("#savemain").attr({
                  disabled: "disabled",
                });
				$("#mainerrors").hide("slow");
				$("#title").removeClass("is-invalid");
				$("#description").removeClass("is-invalid");
				$("#descriptionfull").removeClass("is-invalid");
			}else{	
			   
			    $.each(data.inputs, function(key, value){
                  	if (value == "title") {
						$("#title").addClass("is-invalid");
					}
					if (value == "description") {
						$("#description").addClass("is-invalid");
					}
					if (value == "description-full") {
						$("#descriptionfull").addClass("is-invalid");
					}
                });
				
				$('#mainerrors').append('<strong> Внимание! </strong>');
				$('#mainerrors').append('<ul>');
				$.each(data.errors, function(key, value){
                  	$('#mainerrors').append('<li>'+value+'</li>');
                });
			    $('#mainerrors').append('</ul>');
			    $("#mainerrors").show("slow");
				
			}

        },
        error: function() {
           alert( "Простите, что то пошло не так, перезагрузите страницу и попробуйте еще раз" );
        }
    });
	
	
});

//настройки модулей
function enablem() {
	$('#savemoduls').removeAttr('disabled');
	$("#savemoduls").html("Сохранить");
}

$( "#portfolio" ).change(enablem);
$( "#reviews" ).change(enablem);
$( "#price" ).change(enablem);
$( "#stocks" ).change(enablem);

$("#savemoduls").click(function(){
	$("#loadiconm").show();
	$("#savemoduls").attr({
                  disabled: "disabled",
                });
	
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	if ($('#portfolio').is(':checked')) {var po = 'on';} else {var po = 'off';}
	if ($('#reviews').is(':checked')) {var re = 'on';} else {var re = 'off';}
	if ($('#price').is(':checked')) {var pr = 'on';} else {var pr = 'off';}
	if ($('#stocks').is(':checked')) {var st = 'on';} else {var st = 'off';}
	var nform = "moduls";
	$('#modulserrors').html('');
	$("#modulserrors").hide("slow");
    $.ajax(
    {
        url: "/changeoptions",
        type: 'post', 
        data: {
            portfolio: po,
			price: pr,
			reviews: re,
			stocks: st,			
			form: nform
        },
        success: function (data)
        {    
		    $("#loadiconm").hide();
		    $("#savemoduls").html(data.success);
				$("#savemoduls").attr({
                  disabled: "disabled",
                });
			$("#modulserrors").hide("slow");
        },
        error: function() {
           alert( "Простите, что то пошло не так, перезагрузите страницу и попробуйте еще раз" );
        }
    });
	
	
});

//настройки контактов
function enablec() {
	$('#savecontacts').removeAttr('disabled');
	$("#savecontacts").html("Сохранить");
}

$( "#name_org" ).change(enablec);
$( "#phone_org" ).change(enablec);
$( "#adress_org" ).change(enablec);
$( "#time" ).change(enablec);
$( "#map" ).change(enablec);
$( "#v" ).change(enablec);
$( "#w" ).change(enablec);
$( "#t" ).change(enablec);
$( "#v_on" ).change(enablec);
$( "#w_on" ).change(enablec);
$( "#t_on" ).change(enablec);

$("#savecontacts").click(function(){
	$("#loadiconc").show();
	$("#savecontacts").attr({
                  disabled: "disabled",
                });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var na = $("#name_org").val();
	var ph = $("#phone_org").val();
	var time = $("#time").val();
	var ad = $("#adress_org").val();
	var map = $("#map").val();
	var v = $("#v").val();
	var w = $("#w").val();
	var t = $("#t").val();
	if ($('#v_on').is(':checked')) {var v_on = 'on';} else {var v_on = 'off';}
	if ($('#w_on').is(':checked')) {var w_on = 'on';} else {var w_on = 'off';}
	if ($('#t_on').is(':checked')) {var t_on = 'on';} else {var t_on = 'off';}
	var nform = "contacts";
	
	$('#contactserrors').html('');
	$("#contactserrors").hide("slow");
	$("#name_org").removeClass("is-invalid");
	$("#phone_org").removeClass("is-invalid");
	$("#time").removeClass("is-invalid");
	$("#adress_org").removeClass("is-invalid");
	$("#map").removeClass("is-invalid");
	$("#v").removeClass("is-invalid");
	$("#w").removeClass("is-invalid");
	$("#t").removeClass("is-invalid");
	
    $.ajax(
    {
        url: "/changeoptions",
        type: 'post', 
        data: {
            name_org: na,
			phone_org: ph,
			time: time,
			adress_org: ad,
			v: v,
			w: w,
			t: t,
			map: map,
			v_on: v_on,
			w_on: w_on,
			t_on: t_on,			
			form: nform
        },
        success: function (data)
        {    
		    $("#loadiconc").hide();
       		if($.isEmptyObject(data.errors)){
		        $("#savecontacts").html(data.success);
				$("#savecontacts").attr({
                  disabled: "disabled",
                });
				$("#contactserrors").hide("slow");
				$("#name_org").removeClass("is-invalid");
	            $("#phone_org").removeClass("is-invalid");
	            $("#adress_org").removeClass("is-invalid");
				$("#time").removeClass("is-invalid");
			    $("#map").removeClass("is-invalid");
	            $("#v").removeClass("is-invalid");
	            $("#w").removeClass("is-invalid");
              	$("#t").removeClass("is-invalid");
				
			}else{	
			   
			    $.each(data.inputs, function(key, value){
                  	if (value == "name_org") {
						$("#name_org").addClass("is-invalid");
					}
					if (value == "phone_org") {
						$("#phone_org").addClass("is-invalid");
					}
					if (value == "time") {
						$("#time").addClass("is-invalid");
					}
					if (value == "adress_org") {
						$("#adress_org").addClass("is-invalid");
					}
					if (value == "map") {
						$("#map").addClass("is-invalid");
					}
					if (value == "v") {
						$("#v").addClass("is-invalid");
					}
					if (value == "w") {
						$("#w").addClass("is-invalid");
					}
					if (value == "t") {
						$("#t").addClass("is-invalid");
					}
                });
				
				$('#contactserrors').append('<strong> Внимание! </strong>');
				$('#contactserrors').append('<ul>');
				$.each(data.errors, function(key, value){
                  	$('#contactserrors').append('<li>'+value+'</li>');
                });
			    $('#contactserrors').append('</ul>');
			    $("#contactserrors").show("slow");
				
			}

        },
        error: function() {
           alert( "Простите, что то пошло не так, перезагрузите страницу и попробуйте еще раз" );
        }
    });
	
	
});

// настройки акций
function enables() {
	$('#savestocks').removeAttr('disabled');
	$("#savestocks").html("Сохранить");
}

$( "#stock_visible_before" ).change(enables);


$("#savestocks").click(function(){
	$("#loadicons").show();
	$("#savestocks").attr({
                  disabled: "disabled",
                });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	var stock_visible_before = $("#stock_visible_before").val();
	var nform = "stocks";
	$('#stockserrors').html('');
	$("#stockserrors").hide("slow");
	$("#stock_visible_before").removeClass("is-invalid");
    $.ajax(
    {
        url: "/changeoptions",
        type: 'post', 
        data: {
            stock_visible_before: stock_visible_before,
			form: nform
        },
        success: function (data)
        {    
		    $("#loadicons").hide();
       		if($.isEmptyObject(data.errors)){
		        $("#savestocks").html(data.success);
				$("#savestocks").attr({
                  disabled: "disabled",
                });
				$("#stockserrors").hide("slow");
				$("#stock_visible_before").removeClass("is-invalid");
			}else{	
			   
			    $("#stock_visible_before").addClass("is-invalid");
					
					
				$('#stockserrors').append('<strong> Внимание! </strong>');
				$('#stockserrors').append('<ul>');
				$.each(data.errors, function(key, value){
                  	$('#stockserrors').append('<li>'+value+'</li>');
                });
			    $('#stockserrors').append('</ul>');
			    $("#stockserrors").show("slow");
				
			}

        },
        error: function() {
           alert( "Простите, что то пошло не так, перезагрузите страницу и попробуйте еще раз" );
        }
    });
	
	
});
