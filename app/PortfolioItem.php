<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioItem extends Model
{   
    protected $table = 'portfolioitems';
    protected $fillable = ['title', 'description', 'public', 'image', 'user_id'];
	
}
