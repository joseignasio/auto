<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
      protected $fillable = ['title', 'description', 'date_start', 'date_end', 'image', 'public'];
}
