<?php

namespace App\Http\Controllers;

use Validator;
use App\Price;
use App\Order;
use App\Stock;
use App\PortfolioItem;
use App\Review;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function getPrice()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $items = Price::all();
		
        return view('price', [
		   'items' => $items,
		   'ordersCount' => $ordersCount, 
		   'reviewsCount' => $reviewsCount, 
		   'stocksCount' => $stocksCount,
		   'portfolioItemsCount' => $portfolioItemsCount,
		   'priceItemsCount' => $priceItemsCount
		  ]);
    }
        
    public function storePriceItem(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'title' => 'required|max:255',
             'description' => 'required|max:15000',
              ],
             $messages = [
                  'title.required' => 'Вы не ввели название категории',
                  'title.max' => 'Название категории должно быть менее 255 символов',
                  'description.required' => 'Вы не ввели прайс лист для категории',
                  'description.max' => 'Прайс лист категории должен быть менее 15 000 символов',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/price')
                  ->withErrors($validator, 'newprice')
                  ->withInput();
        }
        $public = $request->public === 'on';
        Price::create(['title' => $request->title, 'description' => $request->description, 'public' => $public ]);
        return redirect('dashboard/price')->with('status', 'Запись успешно добавлена!');
    }
        
    public function changePriceItem(Request $request, Price $item)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'title' => 'required|max:255',
             'description' => 'required|max:15000',
              ],
          $messages = [
                  'title.required' => 'Вы не ввели название категории',
                  'title.max' => 'Название категории должно быть менее 255 символов',
                  'description.required' => 'Вы не ввели прайс лист для категории',
                  'description.max' => 'Прайс лист категории должен быть менее 15 000 символов',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/price')
                  ->withErrors($validator, 'changeprice' . $request->formnumber)
                  ->withInput()
                  ->with('formnumber', $request->formnumber);
        }
          
        $public = $request->public === 'on';
        Price::where('id', $item->id)->update(['title' => $request->title, 'description' => $request->description, 'public' => $public ]); 
        return redirect('dashboard/price')->with('status', 'Запись успешно изменена!');
    }
        
        
        
    public function destroy(Request $request, Price $item)
    {
        $number = $item->id;
        $item->delete();
        return redirect('/dashboard/price');
    }
}
