<?php

namespace App\Http\Controllers;

use App\Price;
use App\PortfolioItem;
use App\Review;
use App\Stock;
use App\Option;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function getHome()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $portfolioItems = PortfolioItem::where('public', true)->get();
        $priceItems = Price::where('public', true)->get();
        $reviews = Review::where('public', true)->get();
         
        // фильтруем акции
        $options = Option::first();
        $optionDate = $options->stock_visible_before;
        $stocks = Stock::all();
        $stocks = $stocks->where('public', true);
        foreach ($stocks as $stock) {
            $start = Carbon::createFromFormat('Y-m-d', $stock->date_start);
            $start->subDays($optionDate);
			$stock->public = $start < Carbon::today()->addDays(1);
        }
        $stocks = $stocks->where('public', true);
         
        return view('welcome', [
		  'portfolioItems' => $portfolioItems, 
		  'priceItems' => $priceItems, 
		  'reviews' => $reviews, 
		  'stocks' => $stocks, 
		  'options' => $options,
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		  ]);
    }
}
