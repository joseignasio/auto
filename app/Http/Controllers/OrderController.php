<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Order;
use App\Stock;
use App\PortfolioItem;
use App\Price;
use App\Review;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function getOrders()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $orders = Order::all();
		
        return view('orders', [
		  'orders' => $orders,
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		]);
    }
        
    public function storeOrder(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
              'name' => 'required|max:255',
              'phone' => 'required|max:30',
              'description' => 'required|max:10000',
              'image' => 'image|file|max:30000',
              ],
             $messages = [
                  'name.required' => 'Вы не ввели имя',
                  'name.max' => 'Похоже у вас слишком длинное имя, напишите пожалуйста сокращенную форму',
                  'phone.required' => 'Укажите свой телефон',
                  'phone.max' => 'Телефон не может быть таким длинным',
                  'description.required' => 'Пожалуйста, опишите свою проблему',
                  'description.max' => 'Пожалуйста, сформулируйте свою проблему короче (максимум 10 000 символов)',
                  'image.image' => 'Загруженный файл должен быть изображением в формате JPEG, PNG, BMP, GIF или SVG',
                  'image.file' => 'Произошла ошибка при загрузке, попробуйте еще раз...',
                  'image.max' => 'Загруженные файлы должны занимать не более 30 мегабайт',
              ]
             );

        if ($validator->fails()) {
            return redirect('/')
                  ->withErrors($validator, 'neworder')
                  ->withInput();
        }
          
        $imgs = [];
    
        foreach ($request->file() as $file) {
            foreach ($file as $f) {
                $img = $f->storeAs('/orders', time().'_'.$f->getClientOriginalName(), 'images');
                $path_img = "img/" . $img;
                $imgs[] = $path_img;
            }
        }
        Order::create([ 'name' => $request->name, 'description' => $request->description, 'phone' => $request->phone, 'image' => $imgs]);
        return redirect('/')->with('status', 'Ваша заявка отправленна!');
    }
    
    public function destroy(Request $request, Order $order)
    {
        $imgs = $order->image;
        foreach ($imgs as $img) {
            $img = str_replace("img/orders/", "", $img);
            $img = 'img\\orders\\' . $img;
            Storage::delete(unlink(public_path($img)));
        }
        $order->delete();
        return redirect('/dashboard/orders');
    }
}
