<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Review;
use App\PortfolioItem;
use App\Price;
use App\Order;
use App\Stock;

class ReviewController extends Controller
{
    public function getReviews()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $reviews = Review::all();
		
        return view('reviews', [
		  'reviews' => $reviews,
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		]);
    }
        
    public function storeReviewByAdmin(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'name' => 'required|max:50',
             'review' => 'required|max:3000',
              ],
             $messages = [
                  'name.required' => 'Вы не ввели имя',
                  'name.max' => 'Похоже у вас слишком длинное имя, напишите пожалуйста сокращенную форму',
                  'review.required' => 'Вы не ввели отзыв',
                  'review.max' => 'Пожалуйста, напишите свой отзыв короче (максимум 3 000 символов)',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/reviews')
                  ->withErrors($validator, 'newreview')
                  ->withInput();
        }
        $public = $request->public === 'on';    
        Review::create([ 'name' => $request->name, 'review' => $request->review, 'public' => $public]);
        return redirect('dashboard/reviews')->with('status', 'Отзыв успешно добавлен!');
    }

    public function storeReviewByAny(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'name' => 'required|max:50',
             'review' => 'required|max:3000',
              ],
             $messages = [
                  'name.required' => 'Вы не ввели имя',
                  'name.max' => 'Похоже у вас слишком длинное имя, напишите пожалуйста сокращенную форму',
                  'review.required' => 'Вы не ввели отзыв',
                  'review.max' => 'Пожалуйста, напишите свой отзыв короче (максимум 3 000 символов)',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/reviews')
                 ->withErrors($validator, 'newreview')
                 ->withInput();
        }
        Review::create([ 'name' => $request->name, 'review' => $request->review, 'public' => false]); 
        return redirect('/')->with('status', 'Отзыв успешно добавлен!');
    }
            
    public function changeReview(Request $request, Review $review)
    {
        $validator = Validator::make(
             $request->all(),
             [
             'name' => 'required|max:50',
             'review' => 'required|max:3000',
              ],
             $messages = [
                  'name.required' => 'Вы не ввели имя',
                  'name.max' => 'Похоже у вас слишком длинное имя, напишите пожалуйста сокращенную форму',
                  'review.required' => 'Вы не ввели отзыв',
                  'review.max' => 'Пожалуйста, напишите свой отзыв короче (максимум 3 000 символов)',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/reviews')
                  ->withErrors($validator, 'changereview' . $request->formnumber)
                  ->withInput()
                  ->with('formnumber', $request->formnumber);
        }
        $public = $request->public === 'on'; 
        $review->update(['name' => $request->name, 'review' => $request->review, 'public' => $public ]);
        return redirect('dashboard/reviews')->with('status', 'Отзыв успешно изменен!');
    }
     
    public function destroy(Request $request, Review $review)
    {
        $review->delete();
        return redirect('/dashboard/reviews');
    }
}
