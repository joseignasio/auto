<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Stock;
use App\PortfolioItem;
use App\Price;
use App\Order;
use App\Review;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function getStocks()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $stocks = Stock::all();
		
        return view('stocks', [
		  'stocks' => $stocks,
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		]);
    }
        
    public function storeStock(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
              'title' => 'required|max:255',
              'description' => 'max:3000',
              'date_start' => 'required|date|after:yesterday',
              'date_end' => 'required|date|after:date_start',
              'image' => 'image|file|max:30000',
              ],
             $messages = [
                  'title.required' => 'Вы не ввели название акции',
                  'title.max' => 'Название акции должно быть менее 255 символов',
                  'description.max' => 'Описание акции должно быть менее 3000 символов',
                  'date_start.required' => 'Введите дату начала акции',
                  'date_start.date' => 'Похоже вы ошиблись с датой начала акции',
                  'date_start.after' => 'Дата начала акции не может быть прошедшей',
                  'date_end.required' => 'Введите дату окончания акции',
                  'date_end.date' => 'Похоже вы ошиблись с датой окончания акции',
                  'date_end.after' => 'Дата окончания акции не может быть раньше даты её начала или быть ей равной',
                  'image.image' => 'Загруженный файл должен быть изображением в формате JPEG, PNG, BMP, GIF или SVG',
                  'image.file' => 'Произошла ошибка при загрузке, попробуйте еще раз...',
                  'image.max' => 'Загруженные файлы должны занимать не более 30 мегабайт',
              ]
             );
        if ($validator->fails()) {
            return redirect('/dashboard/stocks')
                  ->withErrors($validator, 'newstock')
                  ->withInput();
        }   
        $public = $request->public === 'on';  
        if (!(empty($request->image))) {
            $img = $request->file('image')->storeAs('/stocks', time().'_'.$request->file('image')->getClientOriginalName(), 'images');
            $path_img = "img/" . $img;
        } else {
            $path_img = "";
        }
        Stock::create([ 
		  'title' => $request->title,
 		  'description' =>  $request->description, 
		  'date_start' =>  $request->date_start, 
		  'date_end' =>  $request->date_end, 
		  'image' => $path_img, 
		  'public' => $public
		]);   
        return redirect('/dashboard/stocks')->with('status', 'Акция сохранена!');
    }
        
    public function changeStock(Request $request, Stock $stock)
    {
        $validator = Validator::make(
               $request->all(),
               [
              'title' => 'required|max:255',
              'description' => 'max:3000',
              'date_start' => 'required|date',
              'date_end' => 'required|date|after:date_start',
              'image' => 'image|file|max:30000',
              ],
             $messages = [
                  'title.required' => 'Вы не ввели название акции',
                  'title.max' => 'Название акции должно быть менее 255 символов',
                  'description.max' => 'Описание акции должно быть менее 3000 символов',
                  'date_start.required' => 'Введите дату начала акции',
                  'date_start.date' => 'Похоже вы ошиблись с датой начала акции',
                  'date_end.required' => 'Введите дату окончания акции',
                  'date_end.date' => 'Похоже вы ошиблись с датой окончания акции',
                  'date_end.after' => 'Дата окончания акции не может быть раньше даты её начала или быть ей равной',
                  'image.image' => 'Загруженный файл должен быть изображением в формате JPEG, PNG, BMP, GIF или SVG',
                  'image.file' => 'Произошла ошибка при загрузке, попробуйте еще раз...',
                  'image.max' => 'Загруженные файлы должны занимать не более 30 мегабайт',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/stocks')
                  ->withErrors($validator, 'changestock' . $request->formnumber)
                  ->withInput()
                  ->with('formnumber', $request->formnumber);
        }

        $public = $request->public === 'on';
      
        if ($request->file('image') == null) { // если нет фала меняем только текстовые поля, если есть меняем картинку
            Stock::where('id', $stock->id)->update([
			  'title' => $request->title, 
			  'description' =>  $request->description, 
			  'date_start' =>  $request->date_start,  
			  'date_end' =>  $request->date_end,
			  'public' => $public
		    ]);
        } else {
            $filename = DB::table('stocks')->where('id', $stock->id)->value('image');
            $filename = str_replace("img/", "", $filename);
            $filename = 'img\\' . $filename;
            Storage::delete(unlink(public_path($filename)));
            $img = $request->file('image')->storeAs('/stocks', time().'_'.$request->file('image')->getClientOriginalName(), 'images');
            $path_img = "img/" . $img;
            Stock::where('id', $stock->id)->update([
 			   'title' => $request->title, 
			   'description' => $request->description, 
			   'date_start' =>  $request->date_start, 
			   'date_end' =>  $request->date_end,
			   'image' => $path_img, 
			   'public' => $public 
			]);
        }
        return redirect('dashboard/stocks')->with('status', 'Акция успешно изменена!');
    }

    public function destroy(Request $request, Stock $stock)
    {
        $filename = $stock->image;
        if ($filename != null) {
            $filename = str_replace("img/", "", $filename);
            $filename = 'img\\' . $filename;
            Storage::delete(unlink(public_path($filename)));
        }
        $stock->delete();
        return redirect('/dashboard/stocks');
    }
}
