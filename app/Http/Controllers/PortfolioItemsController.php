<?php

namespace App\Http\Controllers;

use Validator;
use App\PortfolioItem;
use App\Price;
use App\Order;
use App\Review;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PortfolioItemsController extends Controller
{
    public function getPortfolio()
    {
		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
      	$priceItemsCount = Price::all()->count(); 
		
        $items = PortfolioItem::all();
		
        return view('portfolio', [
		  'items' => $items, 
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		  ]);
    }
        
    public function storePortfolioItem(Request $request)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'title' => 'required|max:255',
             'description' => 'max:3000',
             'image' => 'required|image|file|max:15000',
              ],
             $messages = [
                  'title.required' => 'Вы не ввели краткое описание',
                  'title.max' => 'Краткое описание должно быть менее 255 символов',
                  'description.max' => 'Полное описание должно быть менее 3000 символов',
                  'image.required' => 'Вы не добавили фото',
                  'image.image' => 'Загруженный файл должен быть изображением в формате JPEG, PNG, BMP, GIF или SVG',
                  'image.file' => 'Произошла ошибка при загрузке, попробуйте еще раз...',
                  'image.max' => 'Загруженный файл должен быть не более 15 мегабайт',
              ]
             );

        if ($validator->fails()) {
            return redirect('dashboard/portfolio')
                  ->withErrors($validator, 'newportfolioitem')
                  ->withInput();
        }
		
        $public = $request->public === 'on';
		
        $img = $request->file('image')->storeAs('/portfolio', time().'_'.$request->file('image')->getClientOriginalName(), 'images');
        $path_img = "img/" . $img;
        PortfolioItem::create([ 
		  'title' => $request->title, 
		  'description' => $request->description,  
		  'image' => $path_img, 
		  'public' => $public
		]);
        return redirect('dashboard/portfolio')->with('status', 'Запись успешно добавлена!');
    }
    
    public function changePortfolioItem(Request $request, PortfolioItem $item)
    {
        $validator = Validator::make(
              $request->all(),
              [
             'title' => 'required|max:255',
             'description' => 'max:3000',
             'image' => 'image|file|max:15000',
              ],
             $messages = [
                  'title.required' => 'Вы не ввели краткое описание',
                  'title.max' => 'Краткое описание должно быть менее 255 символов',
                  'description.max' => 'Полное описание должно быть менее 3000 символов',
                  'image.required' => 'Вы не добавили фото',
                  'image.image' => 'Загруженный файл должен быть изображением в формате JPEG, PNG, BMP, GIF или SVG',
                  'image.file' => 'Произошла ошибка при загрузке, попробуйте еще раз...',
                  'image.max' => 'Загруженный файл должен быть не более 15 мегабайт',
              ]
             );
        if ($validator->fails()) {
            return redirect('dashboard/portfolio')
                  ->withErrors($validator, 'changeportfolioitem' . $request->formnumber)
                  ->withInput()
                  ->with('formnumber', $request->formnumber);
        }
                 
        $public = $request->public === 'on';
		
        // если нет фала меняем только текстовые поля, если есть меняем картинку
        if ($request->file('image') == null) { 
            PortfolioItem::where('id', $item->id)->update([
			  'title' => $request->title, 
			  'description' => $request->description,
			  'public' => $public, 
			]);
        } else {
            $fileName = str_replace("img/", "", $item->image);
            $fileName = 'img\\' . $fileName;
            Storage::delete(unlink(public_path($fileName)));
            $img = $request->file('image')->storeAs('/portfolio', time().'_'.$request->file('image')->getClientOriginalName(), 'images');
            $pathImg = "img/" . $img;
            PortfolioItem::where('id', $item->id)->update([
			  'title' => $request->title, 
			  'description' => $request->description,
			  'public' => $public, 
			  'image' => $pathImg 
			]);
        }
        return redirect('dashboard/portfolio')->with('status', 'Запись успешно изменена!');
    }
             
    public function destroy(Request $request, PortfolioItem $item)
    {
        $fileName = str_replace("img/", "", $item->image);
        $fileName = 'img\\' . $fileName;
        Storage::delete(unlink(public_path($fileName)));
        $item->delete();
        return redirect('/dashboard/portfolio');
    }
}
