<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Option;
use App\PortfolioItem;
use App\Price;
use App\Order;
use App\Review;
use App\Stock;
use Validator;

class OptionController extends Controller
{
    public function getOptions()
    {
    $users = User::all();

		$ordersCount = Order::all()->count();
		$reviewsCount = Review::all()->count(); 
		$stocksCount = Stock::all()->count();
		$portfolioItemsCount = PortfolioItem::all()->count();		
    $priceItemsCount = Price::all()->count(); 
		
    $options = Option::first();
		
    return view('options', [
      'users' => $users,
	  	'options' => $options, 
		  'ordersCount' => $ordersCount, 
		  'reviewsCount' => $reviewsCount, 
		  'stocksCount' => $stocksCount,
		  'portfolioItemsCount' => $portfolioItemsCount,
		  'priceItemsCount' => $priceItemsCount
		]);
    }
    public function setDefaultOptions()
    {
        $mapdef = htmlspecialchars('<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae7b58dd86a4c7c5294a696f719e0aaca07fdd4be9b7bbc4018ab87e33860ae12&amp;width=895&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>', ENT_QUOTES);
		$options = Option::updateOrCreate(['id' => 1], [
		  'title' => "Кузовной ремонт в Петрозаводске", 
		  'description' => "Описание сайта", 
      'description_full' => "Полное описание сайта", 
		  'portfolio' => true,
		  'price' => true, 
		  'reviews' => true,
		  'stocks' => true, 
		  'stock_visible_before' => 0, 
		  'name_org' => 'ООО "Фирма"',
		  'phone_org' => "79214509323", 
		  'time' => 'ПН-ПТ с 09:00 - 17:00',
		  'adress_org' => "г. Петрозаводск", 
		  'map' => $mapdef, 
		  'v' => "79214509323", 
		  'w' => "79214509323", 
		  't' => "ignatnadya", 
		  'v_on' => true, 
		  'w_on' => true, 
		  't_on'=> true
		]);
        return redirect('/dashboard/options')->with('status', 'Установлены настройки по умолчанию');
    }
    public function changeOptions(Request $request)
    {
        $validator_main = Validator::make(
            $request->all(), [
             'title' => 'max:50',
             'description' => 'max:100',
             'descriptionfull' => 'max:1500'
            ],
            $messages = [
              'title.max' => 'Название сайта должно быть менее 50 символов',
              'description.max' => 'Описание сайта должно быть менее 100 символов',
              'descriptionfull.max' => 'Описание сайта должно быть менее 1500 символов'
            ]
        );
        $validator_contacts = Validator::make(
            $request->all(), [
             'name_org' => 'max:100',
             'phone_org' => 'max:10000000000000|numeric',
             'adress_org' => 'max:255',
			 'time' => 'max:255',
			 'map' => 'max:2000',
             'v' => 'max:10000000000000|numeric',
             'w' => 'max:10000000000000|numeric',
             't' => 'max:100',
            ],
            $messages = [
                  'name_org.max' => 'Название организации должно быть менее 100 символов',
                  'phone_org.max' => 'Телефон организации должен быть менее 13 символов',
                  'phone_org.numeric' => 'Телефон организации должен иметь цифровое значение',
				  'time.max' => 'Поле "График работы" должно быть не более 255 символов',
                  'adress_org.max' => 'Адрес организации должно быть менее 300 символов',
				  'map.max' => 'Скрипт карты должен быть менее 2000 символов',
                  'v.max' => 'Телефон Viber должен быть менее 13 символов',
                  'v.numeric' => 'Телефон Viber должен иметь цифровое значение',
                  'w.max' => 'Телефон WhatsApp должен быть менее 13 символов',
                  'w.numeric' => 'Телефон WhatsApp должен иметь цифровое значение',
                  't.max' => 'Имя пользователя Telegram очень длинное',
            ]
        );
        $validator_stocks = Validator::make(
            $request->all(),   [
             'stock_visible_before' => 'min:0|numeric',
            ],
            $messages = [
                 'stock_visible_before.min' => 'Вы ввели отрицательное число',
                 'stock_visible_before.numeric' => 'Число дней до показа акции должно быть числом(да, вот так)',
            ]
        );
        
        $portfolio = $request->portfolio === 'on';
		$price = $request->price === 'on';
        $reviews = $request->reviews === 'on';
		$stocks = $request->stocks === 'on';
		$vOn = $request->v_on === 'on';
		$wOn = $request->w_on === 'on';
		$tOn = $request->t_on === 'on';
		
        $errInputs = [];
        
        if ($request->form == "main") {
            if ($validator_main->fails()) {
                $errInputs[] = ($validator_main->errors()->has('title')) ? "title" : "";
                $errInputs[] = ($validator_main->errors()->has('description')) ? "description" : "";
                $errInputs[] = ($validator_main->errors()->has('descriptionfull')) ? "descriptionfull" : "";
                return response()->json(['errors'=>$validator_main->errors()->all(),'inputs'=> $errInputs]);
            }
            $options = Option::first();
            $options->update([ 
			  'title' =>  $request->title,
			  'description' => $request->description,
        'description_full' => $request->descriptionfull
        			]);
            return response()->json(['success'=>'Сохранено']);
			
        } elseif ($request->form == "moduls") {
            $options = Option::first();
            $options->update([ 
			  'portfolio' => $portfolio, 
			  'price' => $price, 
			  'reviews' => $reviews, 
			  'stocks' => $stocks 
			]);
            return response()->json(['success'=>'Сохранено']);
       
        } elseif ($request->form == "contacts") {
            if ($validator_contacts->fails()) {
				$errInputs[] = ($validator_contacts->errors()->has('name_org')) ? "name_org" : "";
                $errInputs[] = ($validator_contacts->errors()->has('phone_org')) ? "phone_org" : "";
				$errInputs[] = ($validator_contacts->errors()->has('adress_org')) ? "adress_org" : "";
				$errInputs[] = ($validator_contacts->errors()->has('time')) ? "time" : "";
				$errInputs[] = ($validator_contacts->errors()->has('map')) ? "map" : "";
				$errInputs[] = ($validator_contacts->errors()->has('v')) ? "v" : "";
				$errInputs[] = ($validator_contacts->errors()->has('v')) ? "w" : "";
				$errInputs[] = ($validator_contacts->errors()->has('v')) ? "t" : "";
                return response()->json(['errors'=>$validator_contacts->errors()->all(), 'inputs'=> $errInputs]); 
            }
			$map = htmlspecialchars($request->map, ENT_QUOTES);
            $options = Option::first();
            $options->update([ 
			  'name_org' => $request->name_org, 
			  'phone_org' => $request->phone_org, 
			  'time' => $request->time, 
			  'adress_org' => $request->adress_org, 
			  'map' => $map,
			  'v' => $request->v, 
			  'w' => $request->w, 
			  't' => $request->t, 
			  'v_on' => $vOn,
			  'w_on' => $wOn, 
			  't_on'=> $tOn
			]);
            return response()->json(['success'=>'Сохранено']);

        } elseif ($request->form == "stocks") {
            if ($validator_stocks->fails()) {
                return response()->json(['errors'=>$validator_stocks->errors()->all()]);
            }
            $options = Option::first();
            $options->update([
  			  'stock_visible_before' => $request->stock_visible_before
			]);
            return response()->json(['success'=>'Сохранено']);
        }
    }

    public function createUser(Request $request)
    {
       $validator = Validator::make(
              $request->all(),
              [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
              ],
             $messages = [
                  'name.required' => 'Вы не ввели имя',
                  'name.max' => 'Похоже у вас слишком длинное имя, напишите пожалуйста сокращенную форму',
                  'email.email' => 'Некорректный Email',
                  'email.required' => 'Укажите свой Email',
                  'email.max' => 'Email, вероятно, не может быть таким длинным',
                  'email.unique' => 'Пользователь с тамим Email уже зарегистрирован',
                  'password.required' => 'Ведите пароль',
                  'password.min' => 'Пароль должен быть длинее 6 символов',
                  'password.confirmed' => 'Пароли не совпадают'
              ]
             );

        if ($validator->fails()) {
            return back()
                  ->withErrors($validator, 'newuser')
                  ->withInput();
        }
          
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
     
       return redirect('/dashboard/options');
    }


    public function deleteUser(Request $request, User $user)
    {
       if (Auth::user()->id === $user->id) {
         return redirect('/dashboard/options');
       } else {
         $user->delete();
         return redirect('/dashboard/options');
       }
    }

}
