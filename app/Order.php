<?php

namespace App;

use App\Image;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $fillable = ['name', 'phone', 'description', 'image'];
	 protected $casts = [
    'image' => 'array',
  ];
}
