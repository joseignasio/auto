<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['id', 'title', 'description', 'description_full','portfolio', 'price', 'reviews', 'stocks', 'stock_visible_before', 
	                       'name_org', 'phone_org', 'adress_org', 'time', 'map', 'v', 'w', 't', 'v_on', 'w_on', 't_on'];
	
}
