<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::post('/createuser/', 'OptionController@createUser')->middleware('auth');
Route::delete('/deleteuser/{user}', 'OptionController@deleteUser')->middleware('auth');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'HomePageController@getHome')->name('welcome');

Route::get('/dashboard/portfolio', 'PortfolioItemsController@getPortfolio')->name('portfolio')->middleware('auth');
 Route::post('/storeportfolioitem', 'PortfolioItemsController@storePortfolioItem')->middleware('auth');
 Route::post('/changeportfolioitem/{item}', 'PortfolioItemsController@changePortfolioItem')->middleware('auth');
 Route::delete('/deleteportfolioitem/{item}', 'PortfolioItemsController@destroy')->middleware('auth');
             
Route::get('/dashboard/price', 'PriceController@getPrice')->name('price')->middleware('auth');
 Route::post('/storepriceitem', 'PriceController@storePriceItem')->middleware('auth');
 Route::post('/changepriceitem/{item}', 'PriceController@changePriceItem')->middleware('auth');
 Route::delete('/deletepriceitem/{item}', 'PriceController@destroy')->middleware('auth');
 
Route::get('/dashboard/orders', 'OrderController@getOrders')->name('orders')->middleware('auth');
 Route::post('/storeorder', 'OrderController@storeOrder');
 Route::delete('/deleteorder/{order}', 'OrderController@destroy')->middleware('auth');
 
Route::get('/dashboard/reviews', 'ReviewController@getReviews')->name('reviews')->middleware('auth');
 Route::post('/storereviewbyany', 'ReviewController@storeReviewByAny')->name('storereviewbyany');
 Route::post('/storereviewbyadmin', 'ReviewController@storeReviewByAdmin')->middleware('auth');
 Route::post('/changereview/{review}', 'ReviewController@changeReview')->middleware('auth');
 Route::delete('/deletereview/{review}', 'ReviewController@destroy')->middleware('auth');
 
Route::get('/dashboard/stocks', 'StockController@getStocks')->name('stocks')->middleware('auth');
 Route::post('/storestock', 'StockController@storeStock')->middleware('auth');
 Route::post('/changestock/{stock}', 'StockController@changeStock')->middleware('auth');
 Route::delete('/deletestock/{stock}', 'StockController@destroy')->middleware('auth');
 
Route::get('/dashboard/options', 'OptionController@getOptions')->name('options')->middleware('auth');
 Route::get('/setdefaultoptions', 'OptionController@setDefaultOptions')->middleware('auth');
 Route::post('/changeoptions', 'OptionController@changeOptions')->middleware('auth');
