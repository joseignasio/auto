﻿## Установка

- Клонируем репозиторий 

        git clone https://joseignasio@bitbucket.org/joseignasio/auto.git
	
- Создаем базу данных

- Устанавиливаем зависимости

        composer install

- Создаем копию файла .env.example с именем .env

        cp .env.example .env

- Генерируем новый ключ приложения 

        php artisan key:generate

- Изменяем в .env стоки DB_DATABASE, DB_USERNAME, DB_PASSWORD в соответствии с созданной БД

- Проводим миграцию БД

        php artisan migrate

- Устанавливаем начальные настройки сайта

        php artisan db:seed 
	
- Запускаем сервер. Например:

        php artisan serve







