<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title');	
			$table->string('description');
			$table->string('description_full');

			$table->boolean('portfolio');
			$table->boolean('price');
			$table->boolean('reviews');
			$table->boolean('stocks');
			
			$table->integer('stock_visible_before');
			
			$table->string('name_org');
			$table->string('time');
			$table->string('phone_org');
			$table->string('adress_org');
			$table->text('map');
			
			$table->string('v');
			$table->string('w');
			$table->string('t');
			
			$table->boolean('v_on');
			$table->boolean('w_on');
			$table->boolean('t_on');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
