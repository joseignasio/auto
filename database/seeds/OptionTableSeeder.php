<?php

use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
	$mapdef = htmlspecialchars('<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae7b58dd86a4c7c5294a696f719e0aaca07fdd4be9b7bbc4018ab87e33860ae12&amp;width=895&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>', ENT_QUOTES);
   
    DB::table('options')->insert([
       'title' => "Кузовной ремонт в Петрозаводске", 
		  'description' => "Описание сайта", 
      'description_full' => "Полное описание сайта", 
		  'portfolio' => true,
		  'price' => true, 
		  'reviews' => true,
		  'stocks' => true, 
		  'stock_visible_before' => 0, 
		  'name_org' => 'ООО "Фирма"',
		  'phone_org' => "79214509323", 
		  'time' => 'ПН-ПТ с 09:00 - 17:00',
		  'adress_org' => "г. Петрозаводск", 
		  'map' => $mapdef, 
		  'v' => "79214509323", 
		  'w' => "79214509323", 
		  't' => "ignatnadya", 
		  'v_on' => true, 
		  'w_on' => true, 
		  't_on'=> true
    ]);
    }
}
